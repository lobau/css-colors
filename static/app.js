class App {
    constructor() {
        this.NamedColors = {
            AliceBlue: "#F0F8FF",
            AntiqueWhite: "#FAEBD7",
            Aqua: "#00FFFF",
            Aquamarine: "#7FFFD4",
            Azure: "#F0FFFF",
            Beige: "#F5F5DC",
            Bisque: "#FFE4C4",
            Black: "#000000",
            BlanchedAlmond: "#FFEBCD",
            Blue: "#0000FF",
            BlueViolet: "#8A2BE2",
            Brown: "#A52A2A",
            BurlyWood: "#DEB887",
            CadetBlue: "#5F9EA0",
            Chartreuse: "#7FFF00",
            Chocolate: "#D2691E",
            Coral: "#FF7F50",
            CornflowerBlue: "#6495ED",
            Cornsilk: "#FFF8DC",
            Crimson: "#DC143C",
            Cyan: "#00FFFF",
            DarkBlue: "#00008B",
            DarkCyan: "#008B8B",
            DarkGoldenRod: "#B8860B",
            DarkGray: "#A9A9A9",
            DarkGrey: "#A9A9A9",
            DarkGreen: "#006400",
            DarkKhaki: "#BDB76B",
            DarkMagenta: "#8B008B",
            DarkOliveGreen: "#556B2F",
            Darkorange: "#FF8C00",
            DarkOrchid: "#9932CC",
            DarkRed: "#8B0000",
            DarkSalmon: "#E9967A",
            DarkSeaGreen: "#8FBC8F",
            DarkSlateBlue: "#483D8B",
            DarkSlateGray: "#2F4F4F",
            DarkSlateGrey: "#2F4F4F",
            DarkTurquoise: "#00CED1",
            DarkViolet: "#9400D3",
            DeepPink: "#FF1493",
            DeepSkyBlue: "#00BFFF",
            DimGray: "#696969",
            DimGrey: "#696969",
            DodgerBlue: "#1E90FF",
            FireBrick: "#B22222",
            FloralWhite: "#FFFAF0",
            ForestGreen: "#228B22",
            Fuchsia: "#FF00FF",
            Gainsboro: "#DCDCDC",
            GhostWhite: "#F8F8FF",
            Gold: "#FFD700",
            GoldenRod: "#DAA520",
            Gray: "#808080",
            Grey: "#808080",
            Green: "#008000",
            GreenYellow: "#ADFF2F",
            HoneyDew: "#F0FFF0",
            HotPink: "#FF69B4",
            IndianRed: "#CD5C5C",
            Indigo: "#4B0082",
            Ivory: "#FFFFF0",
            Khaki: "#F0E68C",
            Lavender: "#E6E6FA",
            LavenderBlush: "#FFF0F5",
            LawnGreen: "#7CFC00",
            LemonChiffon: "#FFFACD",
            LightBlue: "#ADD8E6",
            LightCoral: "#F08080",
            LightCyan: "#E0FFFF",
            LightGoldenRodYellow: "#FAFAD2",
            LightGray: "#D3D3D3",
            LightGrey: "#D3D3D3",
            LightGreen: "#90EE90",
            LightPink: "#FFB6C1",
            LightSalmon: "#FFA07A",
            LightSeaGreen: "#20B2AA",
            LightSkyBlue: "#87CEFA",
            LightSlateGray: "#778899",
            LightSlateGrey: "#778899",
            LightSteelBlue: "#B0C4DE",
            LightYellow: "#FFFFE0",
            Lime: "#00FF00",
            LimeGreen: "#32CD32",
            Linen: "#FAF0E6",
            Magenta: "#FF00FF",
            Maroon: "#800000",
            MediumAquaMarine: "#66CDAA",
            MediumBlue: "#0000CD",
            MediumOrchid: "#BA55D3",
            MediumPurple: "#9370D8",
            MediumSeaGreen: "#3CB371",
            MediumSlateBlue: "#7B68EE",
            MediumSpringGreen: "#00FA9A",
            MediumTurquoise: "#48D1CC",
            MediumVioletRed: "#C71585",
            MidnightBlue: "#191970",
            MintCream: "#F5FFFA",
            MistyRose: "#FFE4E1",
            Moccasin: "#FFE4B5",
            NavajoWhite: "#FFDEAD",
            Navy: "#000080",
            OldLace: "#FDF5E6",
            Olive: "#808000",
            OliveDrab: "#6B8E23",
            Orange: "#FFA500",
            OrangeRed: "#FF4500",
            Orchid: "#DA70D6",
            PaleGoldenRod: "#EEE8AA",
            PaleGreen: "#98FB98",
            PaleTurquoise: "#AFEEEE",
            PaleVioletRed: "#D87093",
            PapayaWhip: "#FFEFD5",
            PeachPuff: "#FFDAB9",
            Peru: "#CD853F",
            Pink: "#FFC0CB",
            Plum: "#DDA0DD",
            PowderBlue: "#B0E0E6",
            Purple: "#800080",
            Red: "#FF0000",
            RosyBrown: "#BC8F8F",
            RoyalBlue: "#4169E1",
            SaddleBrown: "#8B4513",
            Salmon: "#FA8072",
            SandyBrown: "#F4A460",
            SeaGreen: "#2E8B57",
            SeaShell: "#FFF5EE",
            Sienna: "#A0522D",
            Silver: "#C0C0C0",
            SkyBlue: "#87CEEB",
            SlateBlue: "#6A5ACD",
            SlateGray: "#708090",
            SlateGrey: "#708090",
            Snow: "#FFFAFA",
            SpringGreen: "#00FF7F",
            SteelBlue: "#4682B4",
            Tan: "#D2B48C",
            Teal: "#008080",
            Thistle: "#D8BFD8",
            Tomato: "#FF6347",
            Turquoise: "#40E0D0",
            Violet: "#EE82EE",
            Wheat: "#F5DEB3",
            White: "#FFFFFF",
            WhiteSmoke: "#F5F5F5",
            Yellow: "#FFFF00",
            YellowGreen: "#9ACD32"
        }

        let hashColor = window.location.hash;

        this.initialColor = (hashColor) ? hashColor : (localStorage.getItem("color") || "#dda0dd")
        this.tolerance = 30;
        this.Colors = [];
        this.hexInput = document.getElementById("hexInput");
        this.selectColor = document.getElementById("selectColor");
        this.selectColor.value = this.initialColor;

        this.colorPicker = new iro.ColorPicker("#colorPicker", {
            width: 280,
            color: this.initialColor,
            borderWidth: 3,
            borderColor: "var(--ink)",
            handleRadius: 12
        });

        this.colorPicker.on(["color:init", "color:change"], (color) => {
            this.color = this.colorPicker.color;
            this.update();
            this.hexInput.value = color.hexString;

            if (this.updateTimer) clearTimeout(this.updateTimer);

            this.updateTimer = setTimeout(() => {
                if (window.history.replaceState) {
                    //prevents browser from storing history with each change:
                    window.history.replaceState({}, color.hexString, "/" + color.hexString);
                }

                let colorHash = this.color.hexString.replace("#", "%23");
                document.querySelector("link[rel~='icon']").href = `data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 16 16%22><circle cx=%228%22 cy=%228%22 r=%226%22 fill=%22${colorHash}%22 stroke=%22black%22 stroke-width=%221.5%22 /></svg>`
            }, 100);

        });

        this.hexInput.addEventListener('change', () => {
            this.colorPicker.color.set(this.hexInput.value);
        });

        this.selectColor.addEventListener('change', () => {
            this.colorPicker.color.set(this.selectColor.value);
        });

        Object.keys(this.NamedColors).forEach(color => {
            this.Colors.push({
                name: color,
                color: new iro.Color(this.NamedColors[color])
            });
        });

        this.Colors.sort((a, b) => {
            return a.color.hsl.h - b.color.hsl.h;
        });

        this.Colors.forEach(colorObj => {
            var ratioOnWhite = Math.round(this.contrastRatio({
                r: 255,
                g: 255,
                b: 255
            }, colorObj.color.rgb) * 100) / 100;

            var ratioOnBlack = Math.round(this.contrastRatio({
                r: 0,
                g: 0,
                b: 0
            }, colorObj.color.rgb) * 100) / 100;

            let textColor = (ratioOnBlack > ratioOnWhite) ? "#000" : "#fff";
            let bgColor = (ratioOnBlack < ratioOnWhite) ? "#000" : "#fff";

            let swatch = document.createElement('article');
            swatch.dataset.r = colorObj.color.rgb.r;
            swatch.dataset.g = colorObj.color.rgb.g;
            swatch.dataset.b = colorObj.color.rgb.b;
            swatch.innerHTML = `
          <figure style="background-color: ${colorObj.name}; --active-bg: ${bgColor}; --active-fg: ${textColor};" aria-hidden="true">
              <ul>
                <li onclick="navigator.clipboard.writeText('${colorObj.name}')">${colorObj.name}</li>
                <li onclick="navigator.clipboard.writeText('${colorObj.color.rgbString}')">${colorObj.color.rgbString}</li>
                <li onclick="navigator.clipboard.writeText('${colorObj.color.hslString}')">${colorObj.color.hslString}</li>
                <li onclick="navigator.clipboard.writeText('${colorObj.color.hexString}')">${colorObj.color.hexString}</li>
              </ul>
          </figure>`;

            document.querySelector('main').append(swatch);
        });

        this.update();
    }

    update() {
        let swatches = document.querySelectorAll('article');
        let count = 0;
        swatches.forEach(swatch => {
            let distance = deltaE([swatch.dataset.r, swatch.dataset.g, swatch.dataset.b], [this.color.rgb.r, this.color.rgb.g, this.color.rgb.b]);
            swatch.style.display = distance < 15 ? 'block' : 'none';
            if (distance < 20) {
                count++;
            }
        });

        if (count === 0) {
            nocolors.innerHTML = `There are no CSS named colors that are similar to ${this.color.hexString}.`;
            nocolors.style.display = "block";
        } else {
            nocolors.innerHTML = ``;
            nocolors.style.display = "none";
        }

        var ratioOnWhite = Math.round(this.contrastRatio({
            r: 255,
            g: 255,
            b: 255
        }, this.color.rgb) * 100) / 100;

        var ratioOnBlack = Math.round(this.contrastRatio({
            r: 0,
            g: 0,
            b: 0
        }, this.color.rgb) * 100) / 100;

        let textColor = (ratioOnBlack > ratioOnWhite) ? "#000" : "#fff";
        let backColor = (ratioOnBlack < ratioOnWhite) ? "#000" : "#fff";

        this.selectColor.value = this.color.hexString;
        localStorage.setItem("color", this.color.hexString);


        document.querySelector(':root').style.setProperty('--ink', textColor);
        document.querySelector(':root').style.setProperty('--paper', backColor);
        document.querySelector(':root').style.setProperty('--current', backColor);
        document.querySelector(':root').style.setProperty('--side', this.color.hexString);
    }

    write() {
        return `${this.foo} ${this.bar}`;
    }

    luminanceRatio(r, g, b) {
        var a = [r, g, b].map(function(v) {
            v /= 255;
            return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4);
        });
        return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
    }

    contrastRatio(rgb1, rgb2) {
        var l1 = this.luminanceRatio(rgb1.r, rgb1.g, rgb1.b) + 0.05;
        var l2 = this.luminanceRatio(rgb2.r, rgb2.g, rgb2.b) + 0.05;
        return Math.max(l1, l2) / Math.min(l1, l2);
    }
}

function deltaE(rgbA, rgbB) {
    let labA = rgb2lab(rgbA);
    let labB = rgb2lab(rgbB);
    let deltaL = labA[0] - labB[0];
    let deltaA = labA[1] - labB[1];
    let deltaB = labA[2] - labB[2];
    let c1 = Math.sqrt(labA[1] * labA[1] + labA[2] * labA[2]);
    let c2 = Math.sqrt(labB[1] * labB[1] + labB[2] * labB[2]);
    let deltaC = c1 - c2;
    let deltaH = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
    deltaH = deltaH < 0 ? 0 : Math.sqrt(deltaH);
    let sc = 1.0 + 0.045 * c1;
    let sh = 1.0 + 0.015 * c1;
    let deltaLKlsl = deltaL / (1.0);
    let deltaCkcsc = deltaC / (sc);
    let deltaHkhsh = deltaH / (sh);
    let i = deltaLKlsl * deltaLKlsl + deltaCkcsc * deltaCkcsc + deltaHkhsh * deltaHkhsh;
    return i < 0 ? 0 : Math.sqrt(i);
}

function rgb2lab(rgb) {
    let r = rgb[0] / 255,
        g = rgb[1] / 255,
        b = rgb[2] / 255,
        x, y, z;
    r = (r > 0.04045) ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
    g = (g > 0.04045) ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
    b = (b > 0.04045) ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
    x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
    y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.00000;
    z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;
    x = (x > 0.008856) ? Math.pow(x, 1 / 3) : (7.787 * x) + 16 / 116;
    y = (y > 0.008856) ? Math.pow(y, 1 / 3) : (7.787 * y) + 16 / 116;
    z = (z > 0.008856) ? Math.pow(z, 1 / 3) : (7.787 * z) + 16 / 116;
    return [(116 * y) - 16, 500 * (x - y), 200 * (y - z)]
}

window.onload = () => {
    new App();
}